exports.padNumber = function(number, size) {
  var s = String(number);
  while (s.length < (size || 2)) {s = "0" + s;}
  return s;
}

exports.getPromiseCallback = function () {
  let cb;
  let promise = new Promise((resolve, reject) => {
    cb = function (err, ...args) {
      if (err) return reject(err);
      resolve(args.lenght > 1 ? args : args.shift());
    }
  });

  cb.promise = promise;
  return cb;
}

//returns random number, excluding max
let getRandom = (min, max) => Math.floor(Math.random() * (max-min) + min);
exports.getRandom = getRandom;

//returns some random elements from they
exports.chooseSomeOfThem = (some, they) => {
    let choosen = [];
    for (let i = 0; i < they.length; i++) {
        let rand = getRandom(0, they.length);
        while(choosen.indexOf(they[rand]) !== -1) {
            rand = getRandom(0, they.length);
        }
        choosen.push(they[rand]);
        if (choosen.length >= some) {
            break;
        }
    }
    return choosen;
}
