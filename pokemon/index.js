class Pokemon {
    constructor(name = 'noname', level = 1) {
        this.name = typeof name === 'string' && name || 'villian';
        this.level = typeof level === 'number' && level || 1;
    }

    show() {
        console.log(`Name: ${this.name}, Level: ${this.level}`);
    }

    valueOf() {
        return this.level;
    }

    toString() {
        return `${this.name}(${this.level})`;
    }
}

class PokemonList extends Array {
    constructor(...pokemons) {
        super(...pokemons.filter(pokemon => pokemon instanceof Pokemon));
    }
    add(name, level) {
        this.push(new Pokemon(name, level));
    }
    show() {
        for (let pokemon of this) {
            pokemon.show();
        }
        console.log('____');
        console.log(`Total count: ${this.length}`);
    }
    max() {
        let maxLevel = Math.max(...this);
        return this.find(pokemon => pokemon.level === maxLevel);
    }
}

exports.Pokemon = Pokemon;
exports.PokemonList = PokemonList;
