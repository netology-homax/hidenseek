let fs = require('fs');
let path_module = require('path');
let {padNumber, getPromiseCallback, getRandom, chooseSomeOfThem} = require('./util');
let {PokemonList, Pokemon} = require('./pokemon');

const MAX_POKEMONS_FOR_HIDING = 3;
const NUMBER_OF_ROOMS = 10;
const HIDDEN_FILE_NAME = 'pokemon.txt';
const ROOM_FOLDERS_RIGHTS = '0755';

const ERROR_FOLDER_EXISTS = 'EEXIST';
const ERROR_FILE_EXISTS = 'ENOENT';

const hide = (path, pokemonList) => {
    return new Promise((resolve, reject) => {
        //check path
        path = path_module.normalize(path_module.join(__dirname, path));
        if (path.indexOf(__dirname) != 0) {
            return reject(new Error('Wrong path. It must be inside current directory'));
        }
        if (MAX_POKEMONS_FOR_HIDING > NUMBER_OF_ROOMS) {
            reject(new Error('Not enough rooms for hiding'));
        }

        //select pokemons for hiding
        let hiddenPokemons = choosePokemons(pokemonList);
        let roomsForHiding = chooseRooms(hiddenPokemons.lenght);

        return Promise.resolve()
            .then(() => createRooms(path))
            .then(() => clearRooms(path))
            .then(() => hidePokemons(path, hiddenPokemons, roomsForHiding))
            .catch((err) => reject(err))
            .then(() => {resolve(new PokemonList(...hiddenPokemons))});
    });
};

const seek = (path) => {
    path = path_module.normalize(path_module.join(__dirname, path));
    return readDir(path)
        .then((files) => {
            let promises = files.map(file => readFile(path_module.join(path, file, HIDDEN_FILE_NAME)));
            return Promise.all(promises);
        })
        .then((values) => {
            values = values.filter(value => value);
            let pokemons = values.map(data => {
                let [name, level] = data.split('|');
                return new Pokemon(name, +level);
            });
            return new PokemonList(...pokemons);
        })
        .catch(err => reject(err));
}

function choosePokemons(pokemons) {
    let maxHidden = MAX_POKEMONS_FOR_HIDING <= pokemons.length ? MAX_POKEMONS_FOR_HIDING : pokemons.length;
    let hiddenPokemonsAmount = getRandom(1, maxHidden + 1);
    return chooseSomeOfThem(hiddenPokemonsAmount, pokemons);
}

function chooseRooms(count) {
    let rooms = Array(NUMBER_OF_ROOMS).fill(1);
    return chooseSomeOfThem(count, [...rooms.keys()].map(room => room + 1));
}

function hidePokemons(path, pokemons, rooms) {
    return new Promise((resolve, reject) => {
        let promises = pokemons.map((pokemon, pokemonIndex) => {
            let filepath = path_module.join(path, padNumber(rooms[pokemonIndex]), HIDDEN_FILE_NAME);
            return writeData(filepath, `${pokemon.name}|${pokemon.level}`);
        });
        Promise.all(promises)
            .then(() => resolve())
            .catch(err => reject(err));
    });
}

function writeData(filepath, data) {
    let cb = getPromiseCallback();
    fs.writeFile(filepath, data, cb);
    return cb.promise;
}

function clearRooms(path) {
    return new Promise((resolve, reject) => {
        fs.readdir(path, (err, files) => {
            if (err) {
                return reject(err);
            }
            let promises = files.map(file => clearRoom(path_module.join(path, file, HIDDEN_FILE_NAME)));
            Promise.all(promises)
                .then(() => resolve())
                .catch(err => reject(err));

        });
    });
}

function clearRoom(path) {
    return new Promise((resolve, reject) => {
        fs.lstat(path, (err, stat) => {
            if (err) {
                if (err.code === ERROR_FILE_EXISTS) {
                    resolve();
                }
                return reject(err);
            }
            if (!stat.isFile()) {
                resolve();
            }
            deleteFile(path)
                .then(() => resolve())
                .catch(err => reject(err));
        });
    });
}

function deleteFile(path) {
    let cb = getPromiseCallback();
    fs.unlink(path, cb);
    return cb.promise;
}

function createRooms(path) {
    return createFolderIfNotExists(path)
        .then(() => {
            let roomsPromises = Array(NUMBER_OF_ROOMS).fill().map((_, number) => {
                return createFolderIfNotExists(path_module.join(path, padNumber(number+1)));
            });
            return Promise.all(roomsPromises);
        })
        .catch((err) => {
            throw err;
        });
}

function readDir(path) {
    let cb = getPromiseCallback();
    fs.readdir(path, cb);
    return cb.promise;
}

function readFile(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, { encoding: 'utf-8'}, (err, data) => {
            if (err) {
                if (err.code === ERROR_FILE_EXISTS) {
                    resolve();
                }
                return reject(err);
            }
            resolve(data);
        });
    });
}

function createFolderIfNotExists(path) {
    return new Promise((resolve, reject) => {
        fs.mkdir(path, ROOM_FOLDERS_RIGHTS, function(err) {
            if (err) {
                if (err.code === ERROR_FOLDER_EXISTS) {
                    return resolve();
                }
                return reject(err);
            }
            resolve(path);
        });
    });
}

module.exports = {
    hide,
    seek
};
