let hidenseek = require('./hidenseek');
let {Pokemon, PokemonList} = require('./pokemon');

let [,,command, folder, pokemonsFile] = process.argv;
if (!command) {
    console.log(`
List of commands:

- hide path pokemons-list - прячет в указанной директории некоторое кол-во покемонов из переданного списка и возращает список спрятанных покемонов
- seek path - находит спрятанных покемонов в указанной папке и возвращает их
    `);
} else {
    switch (command) {
        case 'hide':
            if (!folder || !pokemonsFile) {
                console.log("Not all options are specified");
                break;
            }
            let pokemonsRaw = require(pokemonsFile);
            let pokemons = pokemonsRaw.map(pokemonInfo => {
                return new Pokemon(pokemonInfo.name, pokemonInfo.level);
            });
            let pokemonList = new PokemonList(...pokemons);
            hidenseek.hide(folder, pokemonList)
                .then((hiddenPokemonsList) => {
                    console.log(`List of hidden pokemons (${hiddenPokemonsList.length})`);
                    hiddenPokemonsList.show();
                    console.log('\n')
                })
                .catch(err => {
                    console.log('Something went wrong!\n', err.toString());
                });
            break;
        case 'seek':
            if (!folder) {
                console.log("Folder for search is not specified");
                break;
            }
            hidenseek.seek(folder)
                .then((foundPokemonsList) => {
                    console.log(`List of found pokemons (${foundPokemonsList.length})`);
                    foundPokemonsList.show();
                })
                .catch(err => {
                    console.log('Something went wrong!\n', err.toString());
                });
            break;
        default:
            console.error("Command not found", command);
    }
}
